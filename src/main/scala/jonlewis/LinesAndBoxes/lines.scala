package jonlewis.LinesAndBoxes
/*
Some utilities and classes to solve a game of dots and boxes.

a 2x2 lines and boxes model is essentially a binary mapping of spots equal to 2 rows of 3 columns on the horizontal and 3 rows of 2 columns on the vertical more or less
we can just reference these as two sets right, the verticals and the horizontals or V and H
let us say that
| | | 00 01 02
| | | 10 11 12
and that
_ _ 00 10
_ _ 01 11
_ _ 02 12

Typical Usage:
State(2,2).playTurn
*/

case class Position( vertical_? : Boolean, i: Int, j: Int )


class State {
  var width: Int = 0
  var length: Int = 0
  var horizontals: Vector[ Vector[ Boolean ] ] = null
  var verticals: Vector[ Vector[ Boolean ] ] = null
  var scores: Vector[Int] = null
  var player: Int = 0
  var cache: scala.collection.mutable.Map[String,Option[Int]] = null

  // return true if line is filled
  def H( x: Int, y: Int ) = posCheck( horizontals, x, y )
  def V( y: Int, x: Int ) = posCheck( verticals, y, x )
  def posCheck( lines: Vector[Vector[Boolean]], i:Int, j:Int ) = {
    i >= 0 && j >= 0 && i < lines.length && j < lines(0).length && lines(i)(j)
  }

  // determine how many new boxes will be completed if the given position
  // is filled in
  def checkNewBoxes( position: Position ): Int = { 
    val ( primary, opposite ) = if( position.vertical_? ) (verticals,horizontals) else (horizontals,verticals)
    val (i, j) = (position.i, position.j)
    def pc1( i:Int,j:Int) = posCheck( primary, i, j )
    def pc2( i:Int,j:Int) = posCheck( opposite, i, j )
    0 + (if( pc1(i,j-1) && pc2(j-1,i) && pc2(j-1,i+1) ) 1 else 0 ) + ( if( pc1(i, j+1) && pc2(j,i) && pc2( j, i+1 ) ) 1 else 0 )
  }

  // Utility to mark an arbitrary position in a 2d vector as true. 
  def markLine( lines: Vector[Vector[Boolean]], i:Int,j:Int): Vector[Vector[Boolean]] = {
    assert( lines(i)(j) == false )
    lines.updated(i,lines(i).updated(j,true))
  }

  def makeAMove( position: Position ): State = {
    val( newHorizontals, newVerticals ) = if( position.vertical_? ) 
      ( horizontals, markLine( verticals, position.i, position.j ) ) else
      ( markLine( horizontals, position.i, position.j ), verticals )
    val scoreUpdate = checkNewBoxes( position  )
    val newScore = scores.updated( player, scores( player ) + scoreUpdate)
    State( width, length, newHorizontals, newVerticals, newScore, if( scoreUpdate > 0 ) player else ( (player + 1) % scores.length ), cache )
  }



  /*
    Return a list of possible positions that could have lines drawn.
    the solution is quick and dirty and could be improved as a stream or
    something else lazily evaluated in order to keep memory down. On the
    other hand, there will only ever be N (where n is number of lines)
    copies in memory and each copy will be linearly between N and 1 set
    of 2 integers and a boolean. N^2 space savings isn't much.
  */
  def getPossibleMoves: List[Position] = {
    var positions:List[Position] = List()
    var i = 0
    while( i < width ) {
      var j = 0
      while( j < length +1 ) {
        if( !H(i,j) ) positions = (new Position( false, i,j ))::positions
        j=j+1
      }
      i=i+1
    }
    i = 0
    while( i < length ) {
      var j = 0
      while( j < width + 1 ) {
        if( !V(i,j) ) positions = (new Position( true, i,j ))::positions
        j=j+1
      }
      i=i+1
    }
    positions
  }

  def serialize: String = {
    def serializeLines(lines: Vector[Vector[Boolean]]): String = {
      def int(bit:Boolean) = if(bit) 1 else 0
      val result = lines.flatMap(x=>x).foldLeft(0,Seq[Char](),0){
        case ((num: Int, seq: Seq[Char], temp: Int ), bit: Boolean ) if ( num == 8 ) => ( 0, temp.toChar +: seq, int(bit) )
        case ( (num: Int, seq: Seq[Char], temp: Int ), bit: Boolean ) => (num+1, seq, (temp<<1)+int(bit))
      }
      ((result._3).toChar +: result._2).mkString
    }
    List(
      serializeLines(verticals),
      serializeLines(horizontals),
      scores.mkString(""),player
    ).mkString
  }

  // Returns an optioned player number for the winner.
  def playTurn():Option[Int] = {
    val key = serialize
    cache.get(key) match {
      case Some(result) => result
      case None => val result = playTurnInner(); cache.put(key,result); result
    }
  }
  
  def playTurnInner():Option[Int] = {
    // this can be optimized further for pluralities when players > 2
    val winner = scores.findIndexOf( score => score > width * length / 2 )
    if(winner >= 0) return Some(winner)
    // apparently in scala a return will drop you out of an anonymous function through
    // the outer definition. Somewhat unexpected but it works well here.
    val possibleMoves = getPossibleMoves
    // If we have no more moves, determine the winner by who has the highest score, or report a tie
    if( possibleMoves.headOption.isEmpty ) scores.toList.sortWith( (a, b) => a>b ) match {
      case top :: next ::_ if( top == next ) => return None
      case top::_ => return Some(scores.indexOf(top)) 
      case _ => throw new Exception("If you're reading this error you have no players")
    }
    possibleMoves.foldLeft(null:Option[Int])( (bestSoFar,position) => {
      (bestSoFar, makeAMove(position).playTurn ) match {
        case ( _, Some( winner:Int ) ) if(winner == player) => return Some(winner)
        case ( Some( winner:Int ), None ) => None
        case ( null, newValue: Option[Int] ) => newValue
        case ( _, _ ) => bestSoFar
  } } ) }
}

object State {
  // specify all the starting conditions for a new state
  def apply( width: Int, length: Int, horizontals: Vector[Vector[Boolean]], verticals: Vector[Vector[Boolean]], scores: Vector[Int], player: Int, cache: scala.collection.mutable.Map[String,Option[Int]] = scala.collection.mutable.Map() ): State = {
    val s = new State
    s.width = width
    s.length = length
    s.horizontals = horizontals
    s.verticals = verticals
    s.scores = scores
    s.player = player
    s.cache = cache
    // i might pass down the list of available moves given that i can construct it relatively easily based on my current list
    // although i doubt that saves me a lot of time
    s
  }

  // specify just the width and length for a clean start
  def newBoard( width: Int, length: Int, players: Int = 2 ): State = {
    def prepVectors(i:Int, j:Int):Vector[Vector[Boolean]] = {
      val rows = Range(0,j).foldLeft(Vector[Boolean]())((acc,x) => false +: acc )
      Range(0,i).foldLeft(Vector[Vector[Boolean]]())((acc,x) => rows +: acc )
    }
    val horizontals = prepVectors( width, length+1 )
    val verticals = prepVectors( length, width+1 )
    val scores = Range(0,players).foldLeft(Vector[Int]())((acc,x) => 0 +: acc )
    State( width, length, horizontals, verticals, scores, 0 )
  }
}
